import {h, render} from "preact";
import App from "./App";
import { renderComponents } from "./Komponente/Components/Components.jsx";
import {renderHeader} from "./Komponente/Header/Header.jsx"
import { renderPWA } from "./Komponente/PWA/PWA";
import {renderSolution} from "./Komponente/Solution/Solution.jsx"
import {renderDark} from "./Komponente/Dark/Dark"
import {renderMedia} from "./Komponente/Media/Media"

// render(<App />, document.getElementById("root"))
renderHeader(document.getElementById("Header"))
renderSolution(document.getElementById("Solution"))
renderComponents(document.getElementById("Components"))
renderPWA(document.getElementById("PWA"))
renderDark(document.getElementById("Dark"))
renderMedia(document.getElementById("Media"))


