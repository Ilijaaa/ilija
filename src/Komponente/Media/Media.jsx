import {h, render} from "preact";

import styles from "./Media.module.css"

function Media() {
    
    return(
       <div className={`float-left clear-left ml-4 ${styles.div1}`}>
           <h1 className="text-center text-3xl mt-4"><b>AppKit</b></h1>
           <h1 className="text-center  mt-4" >Built to match the design trends and give your page <br></br> the awesome facelift it deserves.</h1>
           <div className="flex justify-center">
          <div className=" float-left m-2  "  style="color:#3b5998"  > <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></div>
          <div className=" float-left m-2 "  style="color:#0d6efd"  > <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></div>
          <div className=" float-left m-2"  style="color:#27ae60"  > <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i></div>
          <div className=" float-left m-2"  style="color:#da4453"  > <i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></div>
          <div className=" float-left m-2"  style="color:#4a89dc"  > <i class="fa fa-square fa-2x" aria-hidden="true"></i></div>
          </div>
          <div className="float-left"></div>
          
          <div className={` float-left clear-left -ml-px  ${styles.lin1} m-4`} ></div>
          <div className={`${styles.dva}`}>
                <div className={`float-left ${styles.txt}`}>Privacy Policy</div>
          </div>

          <div className={`${styles.dva}`}>
                <div className={`float-left ${styles.txt}`}>Tearms of Service</div>
          </div>

          <div className={`${styles.dva}`}>
                <div className={`float-left ${styles.txt}`}>Contact Support</div>
          </div>
          
       </div>
          
       
    )
}

export default Media

export const renderMedia = (args) => render(<Media />, args)