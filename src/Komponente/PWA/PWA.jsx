import {h, render} from "preact";

import styles from "./PWA.module.css"

function PWA() {
    
    return(
       <div>
          <div className={`float-left clear-left m-5 p-2  ${styles.div1}`}>
             
             
              <div className="float-left m-2" style="color:#a0d468">
              <i class="fa fa-home fa-3x"></i>
              
            </div>

            <div>
              <div className="float-left  text-lg">
                 <b> PWA <br></br> Ready
                   </b>
              </div>
              <p className={`float-left clear-left text-xs mt-2 ${styles.text}`}>
                  Enjoy AppKit from your home screen.
              </p>
              
              
            </div> 

            
              





           


          </div>

          <div className={`float-right  m-5 p-2  ${styles.div2}`}>
          <div className="float-left m-2" style="color:#4a89dc">
              <i class="fa fa-cog fa-3x"></i>
              
            </div>

            <div>
              <div className="float-left  text-lg">
                 <b> Clean <br></br> Code
                   </b>
              </div>
              <p className={`float-left clear-left text-xs mt-2 ${styles.text}`}>
                  Enjoy AppKit from your home screen.
              </p>
              
              
            </div> 
          </div>
          

       </div>
    )
}

export default PWA
export const renderPWA = (args) => render(<PWA />, args)
