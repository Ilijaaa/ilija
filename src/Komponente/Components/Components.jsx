import {h, render} from "preact";
import styles from "./Components.module.css"
function Components() {
    
    return(
       <div>
           <div className={`float-left bg-white mt-5 ${styles.div1}`}>
        <div className=" float-left ">
           <img src="https://www.enableds.com/products/appkit/v22/images/pictures/31l.jpg" className={`float-left h-48 m-2 w-32 ${styles.Borderadius}`}  />
           
           <div className="float-left">

           <p className={`float-left   font-semibold text-sm mt-4 ${styles.boja1}`}>Copy and Paste Ready</p>
           <h1 className={"float-left clear-left font-bold  text-2xl  "}>Components</h1>
           <p className={`float-left text-xs mt-4 clear-left  ${styles.boja2}`}>Reusable components that are just as easy as copy and paste.</p>
           <div className="mt-8 float-left clear-left">
           <a href="#" className={`${styles.button}`}><b>Veiw All</b></a>
           </div>
           </div>
           
        </div>
        
        <div className={` float-left  ${styles.lin1} m-4`} ></div>
        

        <div className="float-left clear-left">

           <div className="float-left">

           <img src="https://www.enableds.com/products/appkit/v22/images/pictures/6l.jpg" className={`float-left h-48 m-2 w-32 ${styles.Borderadius}`}  />
           <div className="float-left">
           <p className={`float-left font-semibold text-sm mt-4 ${styles.boja1}`}>Endless Options</p>
           <h1 className={"float-left clear-left   font-bold  text-2xl "}>Homepages</h1>
           <p className={`float-left clear-left  text-xs mt-4   ${styles.boja2}`}>Absolutely awesome homepages ready for you to enjoy.</p>
           <div className="mt-8 float-left clear-left">
           <a href="#" className={`${styles.button}`}><b>Veiw All</b></a>
           </div>
           </div>
           </div>

        </div>

        <div className={` float-left  ${styles.lin1} m-4`} ></div>

        <div className="float-left clear-left">

        <div className="float-left">

           <img src="https://www.enableds.com/products/appkit/v22/images/pictures/16l.jpg" className={`float-left h-48 m-2 w-32 ${styles.Borderadius}`}  />
           <div className="float-left">
           <p className={`float-left font-semibold text-sm mt-4 ${styles.boja1}`}>Beautifully Crafted</p>
           <h1 className={"float-left clear-left  font-bold  text-2xl  "}>Pages</h1>
           <p className={`float-left clear-left  text-xs mt-4 ${styles.boja2}`}>Pages that feel amazing to your fingertips! Ready to use!</p>
           <div className="mt-8 float-left clear-left">
           <a href="#" className={`${styles.button}`}><b>Veiw All</b></a>
           </div>
           </div>
           </div>

        </div>
        
        
        
        </div>
       </div>
    )
}

export default Components
export const renderComponents = (args) => render(<Components />, args)
