import {h, render} from "preact";
import styles from "./Header.module.css"

function Header() {
    
    return(
        <div className="float-left">
          <div className="float-left"> <img src="https://www.enableds.com/products/appkit/v22/images/pictures/16.jpg" className={`h-56 mb-8 ${styles.slika1}`} /></div> 
          <div className="float-left"> <img src="https://www.enableds.com/products/appkit/v22/images/pictures/19.jpg" className={`h-56 mb-8 ml-4 ${styles.slika1}`} /></div>  
        </div>
    )
}

export default Header

export const renderHeader = (args) => render(<Header />, args)
