import {h, render} from "preact";

import styles from "./Solution.module.css"

function Solution() {
    
    return(
       <div>

           <div className={`float-left clear-left m-5 p-2 ${styles.div1}`}>
                <p className={`float-left clear-left font-semibold, text-sm mt-2 ${styles.boja1}`}><b>A Complete Solution</b></p>
                <h1 className="float-left clear-left text-2xl mb-4" ><b>Meet Appkit</b></h1>
                <p className={`float-left clear-left text-xs ${styles.mabo}`}>The ultimate Mobile Solution for your next mobile project. Appkit is packed with hundreds of reusable components, PWA, RTL and it's Dark Mode ready.</p>
               
           </div>

       </div>
    )
}

export default Solution 
export const renderSolution = (args) => render(<Solution />, args)
