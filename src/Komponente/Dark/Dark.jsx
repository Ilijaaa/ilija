import {h, render} from "preact";

import styles from "./Dark.module.css"

function Dark() {
    
    return(
       <div className={`float-left clear-left m-5 p-4 ${styles.div1}` } >
        
        
        <div className="float-left"  style="color:#aab2bd">
            
        <i class="fa  fa-moon-o fa-3x"></i>
        </div>
          
         <div className="float-left">
             
         <p className={`float-left ${styles.boja1}`}><h4>Tap to Enable</h4></p>
         <p className={`float-left clear-left text-lg `}><h1><b>Dark Mode</b></h1></p>
         
        </div> 
        </div>
          

       
    )
}

export default Dark
export const renderDark = (args) => render(<Dark />, args)
