import {h} from "preact";
import Header from "./Komponente/Header/Header"
import Solution from "./Komponente/Solution/Solution"
import Components from "./Komponente/Components/Components"
import PWA from "./Komponente/PWA/PWA"
import Dark from "./Komponente/Dark/Dark"
import Media from "./Komponente/Media/Media"


function App() {
    return (
        <Header/>,
        <Solution/>,
        <Components/>,
        <PWA/>,
        <Dark/>,
        <Media/>
    
    )
}

export default App;